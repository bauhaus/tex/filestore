FROM node:10-alpine as base

WORKDIR /app

FROM base as app

RUN apk add --no-cache git python3 make
RUN git clone https://gitlab.com/bauhaus/tex/mirror/filestore.git /app/

RUN npm ci --quiet

FROM node:10-alpine

RUN apk add --no-cache tini ghostscript imagemagick optipng
ENTRYPOINT ["/sbin/tini", "--"]

COPY --from=app /app /app

WORKDIR /app
RUN chown node:node uploads user_files template_files

COPY ./settings.js /app/config/settings.js
ENV SHARELATEX_CONFIG=/app/config/settings.js

USER node

CMD ["node", "--expose-gc", "app.js"]
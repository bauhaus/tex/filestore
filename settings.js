module.exports = {
  internal: {
    filestore: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
  filestore: {
    backend: process.env["BACKEND"] || "fs",
    stores: {
      user_files: process.env["USER_FILES_BUCKET_NAME"] || "/app/user_files",
      template_files:
        process.env["TEMPLATE_FILES_BUCKET_NAME"] || "/app/template_files",
      public_files: process.env["PUBLIC_FILES_BUCKET_NAME"] || "/app/uploads",
    },
  },
};
